package ru.turubarov.conference.bean;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Employee.class)
public abstract class Employee_ {

	public static volatile SingularAttribute<Employee, Date> birthday;
	public static volatile SingularAttribute<Employee, Unit> unit;
	public static volatile SetAttribute<Employee, Conference> conferenceResponce;
	public static volatile SingularAttribute<Employee, Long> id;
	public static volatile SetAttribute<Employee, Conference> conferenceParticipare;
	public static volatile SingularAttribute<Employee, String> fio;

}

