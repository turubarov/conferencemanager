package ru.turubarov.conference.bean;

import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Unit.class)
public abstract class Unit_ {

	public static volatile SingularAttribute<Unit, String> name;
	public static volatile SingularAttribute<Unit, Long> id;
	public static volatile SetAttribute<Unit, Employee> employees;
	public static volatile SetAttribute<Unit, Conference> conferences;

}

