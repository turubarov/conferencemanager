package ru.turubarov.conference.bean;

import java.sql.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SetAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Conference.class)
public abstract class Conference_ {

	public static volatile SingularAttribute<Conference, Date> date;
	public static volatile SingularAttribute<Conference, String> subject;
	public static volatile SingularAttribute<Conference, Unit> organizer;
	public static volatile SingularAttribute<Conference, Employee> responsible;
	public static volatile SingularAttribute<Conference, Long> id;
	public static volatile SetAttribute<Conference, Employee> participants;

}

