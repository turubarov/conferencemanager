<%@page import="ru.turubarov.conference.view.ConferenceView"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ru.turubarov.conference.services.EmployeeService"%>
<%@page import="ru.turubarov.conference.services.ConferenceService"%>
<%@page import="ru.turubarov.conference.services.UnitService"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Список совещаний</title>
</head>
<body>
	<%@ page import="java.util.List, java.util.Set, ru.turubarov.conference.dao.*, ru.turubarov.conference.bean.*,
	java.text.SimpleDateFormat"%>
	<%
		UnitService unitService = new UnitService();
		List<Unit> units = unitService.findAll();
		
		EmployeeService employeeService = new EmployeeService();
		List<Employee> employees = employeeService.findAll();
		
		ConferenceService conferenceService = new ConferenceService();
		List<Conference> conferences = null;
		ConferenceView cView = null;
		if (request.getAttribute("conferences") != null) {
			conferences = (List<Conference>) request.getAttribute("conferences");
		} else {
			conferences = conferenceService.findAll();
		}
		if (request.getAttribute("conferenceView") != null) {
			cView = (ConferenceView) request.getAttribute("conferenceView");
		}
	%>
	<form action="GetConferenceServlet" method="POST">
		Тема: 
		<% if (cView == null || cView.getSubject() == "") {%>
			<input name="subject" type="text" size="50" />
		<%} else { %>
			<input name="subject" type="text" size="50" value="<%=cView.getSubject()%>"/>
		<%} %> 
		<p/>
		Подразделение: 
		<select name="unit_id">
		<% if (cView == null || cView.getIdUnit() == -1) {%>
			<option value="-1" selected="selected"></option>
		<% } else {%>
			<option value="-1" ></option>
		<% } %>
		<%
			for (int i = 0; i < units.size(); i++) {
				Unit unit = units.get(i);
				if (cView == null || cView.getIdUnit() != unit.getId()) {
				%>
					<option value="<%=unit.getId()%>"><%=unit.getName() %></option>
				<% } else if (cView != null && cView.getIdUnit() == unit.getId()) {%>
					<option value="<%=unit.getId()%>" selected="selected"><%=unit.getName() %></option>
				<% } %>
				<%
				}
			%>
		</select>
		<p/>
		С участием: 
		<select name="employee_id">
		<option value="-1" selected="selected"></option>
		<%
			for (int i = 0; i < employees.size(); i++) {
				Employee employee = employees.get(i);
				if (cView == null || cView.getIdEmployee() != employee.getId()) {
			%>
				<option value="<%=employee.getId()%>"><%=employee.getFio() %></option>
			<%} else if (cView != null && cView.getIdEmployee() == employee.getId()){%>
				<option value="<%=employee.getId()%>"  selected="selected"><%=employee.getFio() %></option>
			<%} %>
			<%
		}
		%>
		</select>
		<p/>
		Даты: 
		<% if (cView == null || cView.getStartOfRange() == null) {%>
			<input name="start_of_range" type="date" />
		<%} else {  %>
			<input name="start_of_range" type="date" value="<%=cView.getStringStartOfRange()%>"/>
		<%} %> 
		
		<% if (cView == null || cView.getEndOfRange() == null) {%>
			<input name="end_of_range" type="date" />
		<%} else {  %>
			<input name="end_of_range" type="date" value="<%=cView.getStringEndOfRange()%>"/>
		<%} %> 
		<p/>
		<input type="Submit" value="SELECT"/>
	</form>
	<table border="1">
	<tr>
		<td>Тема совещания</td>
		<td>Дата</td>
		<td>Подразделение</td>
		<td>Ответственный</td>
		<td>Участники</td>
	</tr>
		<%
			for(int i = 0; i < conferences.size(); i++) {
		           Conference conference = conferences.get(i) ;
		           Set<Employee> set = conference.getParticipants();
		%>
		<tr>
			<td><a href="UpdateConferenceServlet?conferenceId=<%=conference.getId() %>"><%=conference.getSubject()%></a></td>
			<td><%=conference.getDate()%></td>
			<td><%=conference.getOrganizer().getName()%></td>
			<td><%=conference.getResponsible().getFio()%></td>
			<td><%=String.valueOf(conference.getParticipants().size())%></td>
		</tr>
		<%
			}
		%>
	</table>
</body>
</html>