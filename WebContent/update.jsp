<%@page import="java.util.HashSet"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Редактировать совещание</title>
</head>
<body>
	<%@ page import="java.util.List, java.util.Set, ru.turubarov.conference.services.*, ru.turubarov.conference.dao.*, ru.turubarov.conference.bean.*,
	java.text.SimpleDateFormat"%>
	<%
		ConferenceService service = new ConferenceService();
		Conference updating = (Conference)request.getAttribute("conference");
	
		UnitService unitService = new UnitService();
		List<Unit> units = unitService.findAll();
		
		EmployeeService employeeService = new EmployeeService();
		List<Employee> employees = employeeService.findAll();
		Set<Employee> curEmployees = updating.getParticipants();
		
		Set<Employee> notPartEmployees = new HashSet<Employee>(employees);
		notPartEmployees.removeAll(curEmployees);
	%>
	<form action="SaveConferenceServlet" method="POST">
	<input type="hidden" name="conference_id" value="<%=updating.getId()%>">
	Тема:
	<input name="subject" type="text" value="<%=updating.getSubject()%>" size="50" />
	<p/>
	Время проведения
	<input name="date_of_conference" type="date" value="<%=updating.getStringOfDate() %>"/>
	<p/>
	Подразделение
	<select name="unit_id">
		<% for (int i = 0; i < units.size(); i++) {
			Unit unit = units.get(i);
			if (updating.getOrganizer().getId() != unit.getId()) {
			%>
				<option value="<%=unit.getId()%>"><%=unit.getName() %></option>
			<% } else {%>
				<option value="<%=unit.getId()%>" selected="selected"><%=unit.getName() %></option>
			<% } %>
			<%
			}
		%>
	</select>
	<p/>
	Ответственный
	<select name="employee_id">
		<% for (int i = 0; i < employees.size(); i++) {
			Employee employee = employees.get(i);
			if (updating.getResponsible().getId() != employee.getId()) {
			%>
				<option value="<%=employee.getId()%>"><%=employee.getFio() %></option>
			<% } else {%>
				<option value="<%=employee.getId()%>" selected="selected"><%=employee.getFio() %></option>
			<% } %>
			<%
			}
		%>
	</select>	
	<p/>
	<input type="Submit" value="SAVE"/>
	</form>
	Участники
	<table border="1">
	<tr>
		<td>Имя</td>
		<td>Возраст</td>
		<td>Подразделение</td>
		<td></td>
	</tr>
		<%
			
			for (Employee employee: curEmployees) {
		%>
		<tr>
			<td><%=employee.getFio()%></td>
			<td><%=employee.getAge()%></td>
			<td><%=employee.getUnit().getName()%></td>
			<td><a href="RemoveEmployeeServlet?conferenceId=<%=updating.getId()%>&employeeId=<%=employee.getId()%>">удалить</a></td>
		</tr>
		<%
			}
		%>
	</table>
	<p/>
	<form action="AddEmployeeServlet" method="POST">
	Сотрудники <br/>
	<input type="hidden" name="conference_id" value="<%=updating.getId()%>">
	<select name="add_employee_id">
			<%for (Employee employee: notPartEmployees) {
			%>
				<option value="<%=employee.getId()%>"><%=employee.getFio() %></option>
			<%} %>
			<input type="Submit" value="Добавить"/>
	</select>
	</form>
</body>
</html>