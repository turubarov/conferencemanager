package ru.turubarov.conference.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.turubarov.conference.services.ConferenceService;

/**
 * Servlet implementation class AddEmployeeServlet
 */
public class AddEmployeeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddEmployeeServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long conferenceId = Long.parseLong(request.getParameter("conference_id"));
		long employeeId = Long.parseLong(request.getParameter("add_employee_id"));
		ConferenceService service = new ConferenceService();
		service.addEmployeeToConference(conferenceId, employeeId);
		
		request.setAttribute("conference", service.findById(conferenceId));
		request.getRequestDispatcher("add.jsp").forward(request, response);
	}

}
