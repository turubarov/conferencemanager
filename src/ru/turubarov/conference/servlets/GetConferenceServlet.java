package ru.turubarov.conference.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.turubarov.conference.bean.Conference;
import ru.turubarov.conference.services.ConferenceService;
import ru.turubarov.conference.view.ConferenceView;

/**
 * Servlet implementation class GetBookServlet
 */
public class GetConferenceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetConferenceServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// ��������� ��������� ��� ������������ ����������� ������� ����
		request.setCharacterEncoding("utf-8");
		// ���������� � ������� ������ �� ���������� �������
		int unitId = Integer.parseInt(request.getParameter("unit_id"));
		int employeeId = Integer.parseInt(request.getParameter("employee_id"));
		String subject = request.getParameter("subject");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date startOfRange = null;
		Date endOfRange = null;
		try {
			startOfRange = format.parse(request.getParameter("start_of_range"));
		} catch (ParseException e) {
			startOfRange = null;
		}
		try {
			endOfRange = format.parse(request.getParameter("end_of_range"));
		} catch (ParseException e) {
			endOfRange = null;
		}
		
		
		// ���������� �������� ������
		ConferenceView cView = new ConferenceView();
		cView.setSubject(subject);
		cView.setIdUnit(unitId);
		cView.setIdEmployee(employeeId);
		cView.setStartOfRange(startOfRange);
		cView.setEndOfRange(endOfRange);
		
		// ��������� ������ ��������� �� �������
		ConferenceService service = new ConferenceService();
		List<Conference> conferences = service.findByParameters(cView);
		
		request.setAttribute("conferences", conferences);
		request.setAttribute("conferenceView", cView);
		
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

}
