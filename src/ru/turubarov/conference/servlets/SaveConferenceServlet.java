package ru.turubarov.conference.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ru.turubarov.conference.services.ConferenceService;

/**
 * Servlet implementation class SaveConferenceServlet
 */
public class SaveConferenceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SaveConferenceServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int id = Integer.parseInt(request.getParameter("conference_id"));
		int unitId = Integer.parseInt(request.getParameter("unit_id"));
		int employeeId = Integer.parseInt(request.getParameter("employee_id"));
		String subject = request.getParameter("subject");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date dateOfConference = null;
		try {
			dateOfConference = format.parse(request.getParameter("date_of_conference"));
		} catch (ParseException e) {
			dateOfConference = null;
		}
		ConferenceService service = new ConferenceService();
		service.updateConference(id, unitId, employeeId, subject, dateOfConference);
		
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

}
