package ru.turubarov.conference.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;

import org.apache.naming.java.javaURLContextFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;

import ru.turubarov.conference.bean.Conference;
import ru.turubarov.conference.bean.Conference_;
import ru.turubarov.conference.bean.Employee;
import ru.turubarov.conference.bean.Employee_;
import ru.turubarov.conference.bean.Unit;
import ru.turubarov.conference.view.ConferenceView;

/**
 * ����� ��� ������� � ������ � ���������� {@link Conference}.
 * 
 * @author ���������
 * 
 */
public class ConferenceDAO {
	private Session currentSession;
	private Transaction currentTransaction;

	public Session openCurrentSession() {
		currentSession = getSessionFactory().openSession();
		return currentSession;
	}

	public Session openCurrentSessionwithTransaction() {
		currentSession = getSessionFactory().openSession();
		currentTransaction = currentSession.beginTransaction();
		return currentSession;
	}

	public void closeCurrentSession() {
		currentSession.close();
	}

	public void closeCurrentSessionwithTransaction() {
		currentTransaction.commit();
		currentSession.close();
	}

	public Session getCurrentSession() {
		return currentSession;
	}

	public void setCurrentSession(Session currentSession) {
		this.currentSession = currentSession;
	}

	public Transaction getCurrentTransaction() {
		return currentTransaction;
	}

	public void setCurrentTransaction(Transaction currentTransaction) {
		this.currentTransaction = currentTransaction;
	}

	private static SessionFactory getSessionFactory() {
		final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.configure().build();
		try {
			return new MetadataSources(registry).buildMetadata()
					.buildSessionFactory();
		} catch (Exception ex) {
			StandardServiceRegistryBuilder.destroy(registry);
			return null;
		}
	}
	
	public void addEmployeeToConference(long confererenceId, long employeeId) {
		Conference conference = currentSession.load(Conference.class, confererenceId);
		Employee employee = currentSession.load(Employee.class, employeeId);
		conference.getParticipants().add(employee);
		employee.getConferenceParticipare().add(conference);
		currentSession.update(conference);
		currentSession.update(employee);
	}
	
	public void removeEmployeeToConference(long confererenceId, long employeeId) {
		Conference conference = currentSession.load(Conference.class, confererenceId);
		Employee employee = currentSession.load(Employee.class, employeeId);
		conference.getParticipants().remove(employee);
		employee.getConferenceParticipare().remove(conference);
		currentSession.update(conference);
		currentSession.update(employee);
	}

	/**
	 * �������� ������ ���� ���������.
	 * 
	 * @return
	 */
	public List<Conference> findAll() {
		CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Conference> query = builder.createQuery(Conference.class);
		Root<Conference> root = query.from(Conference.class);
		query.select(root);
		Query<Conference> q = getCurrentSession().createQuery(query);
		return q.getResultList();
	}
	
	/**
	 * �������� ��������� � �������� ���������������.
	 * @param id
	 * @return
	 */
	public Conference findById(long id) {
		CriteriaBuilder builder = getCurrentSession().getCriteriaBuilder();
		CriteriaQuery<Conference> query = builder.createQuery(Conference.class);
		Root<Conference> root = query.from(Conference.class);
		query.select(root).where(builder.equal(root.get(Conference_.id), id));
		Query<Conference> q = getCurrentSession().createQuery(query);
		return q.getSingleResult();
	}


	public List<Conference> findByParticipareId(long employeeId) {
		EntityManager em = getCurrentSession().getEntityManagerFactory()
				.createEntityManager();

		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<Conference> criteriaQuery = criteriaBuilder
				.createQuery(Conference.class);

		Root<Employee> answerRoot = criteriaQuery.from(Employee.class);
		criteriaQuery.where(criteriaBuilder.equal(answerRoot.get(Employee_.id),
				employeeId));
		SetJoin<Employee, Conference> answers = answerRoot
				.join(Employee_.conferenceParticipare);
		CriteriaQuery<Conference> cq = criteriaQuery
				.select(answers);

		 TypedQuery<Conference> query = em.createQuery(cq);
		 return query.getResultList();
	}
	
	public void updateConference(long id, long unitId, long employeeId, 
			String subject, Date dateOfConference) {
		Conference conference = new Conference();
		conference.setId(id);
		conference.setOrganizer(currentSession.load(Unit.class, unitId));
		conference.setResponsible(currentSession.load(Employee.class, employeeId));
		conference.setSubject(subject);
		conference.setDate(new java.sql.Date(dateOfConference.getTime()));
		currentSession.update(conference);
	}

	public List<Conference> findByParameters(ConferenceView cView) {
		EntityManager entityManager = getCurrentSession()
				.getEntityManagerFactory().createEntityManager();
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery<Conference> criteriaQuery = criteriaBuilder
				.createQuery(Conference.class);

		Root<Conference> root = criteriaQuery.from(entityManager.getMetamodel()
				.entity(Conference.class));

		Predicate startDatePredicate;
		Predicate endDatePredicate;
		Predicate unitIdPredicate;
		Predicate subjectPredicate;
		final Predicate TRUE = criteriaBuilder.and();
		
		if (cView.getStartOfRange() != null) {
			startDatePredicate = criteriaBuilder.greaterThanOrEqualTo(
					root.get(Conference_.date).as(Date.class),
					cView.getStartOfRange());
		} else {
			startDatePredicate = TRUE;
		}

		if (cView.getEndOfRange() != null) {
			endDatePredicate = criteriaBuilder.lessThanOrEqualTo(
					root.get(Conference_.date).as(Date.class),
					cView.getEndOfRange());
		} else {
			endDatePredicate = TRUE;
		}
		if (cView.getIdUnit() != -1) {
			unitIdPredicate = criteriaBuilder.equal(
					root.get(Conference_.organizer), cView.getIdUnit());
		} else {
			unitIdPredicate = TRUE;
		}
		if (!cView.getSubject().isEmpty()) {
			subjectPredicate = criteriaBuilder.like(
					root.get(Conference_.subject), "%"+cView.getSubject()+"%");
		} else {
			subjectPredicate = TRUE;
		}

		Predicate and = criteriaBuilder.and(startDatePredicate,
				endDatePredicate, unitIdPredicate, subjectPredicate);
		criteriaQuery.where(and);

		List<Conference> list = entityManager.createQuery(criteriaQuery)
				.getResultList();

		return list;
	}
	
	
}
