package ru.turubarov.conference.bean;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * ����� ���������.
 * 
 * @author ���������
 * 
 */

@Entity
@Table(name = "conference")
public class Conference {
	private long id;
	private String subject;
	private Date date;
	private Unit organizer;
	private Employee responsible;
	private Set<Employee> participants;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	/**
	 * ���� ���������.
	 * 
	 * @return
	 */
	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * ���� ���������.
	 * 
	 * @return
	 */
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * �������������-����������� ���������.
	 * 
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "id_organizer")
	public Unit getOrganizer() {
		return organizer;
	}

	public void setOrganizer(Unit organizer) {
		this.organizer = organizer;
	}

	/**
	 * ���������, ������������� �� ���������.
	 * @return
	 */
	@ManyToOne
	@JoinColumn(name = "id_responsible")
	public Employee getResponsible() {
		return responsible;
	}

	public void setResponsible(Employee responsible) {
		this.responsible = responsible;
	}

	/**
	 * ��������� ���������� ���������.
	 * @return
	 */
	@ManyToMany(fetch = FetchType.EAGER, mappedBy = "conferenceParticipare")
	public Set<Employee> getParticipants() {
		return participants;
	}

	public void setParticipants(Set<Employee> participants) {
		this.participants = participants;
	}
	
	@Transient
	public String getStringOfDate () {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

}
