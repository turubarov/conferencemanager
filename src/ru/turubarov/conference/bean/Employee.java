package ru.turubarov.conference.bean;

import java.sql.Date;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "employee")
public class Employee {
	private long id;
	private String fio;
	private Date birthday;
	private Unit unit;
	private Set<Conference> conferenceResponce;
	private Set<Conference> conferenceParticipare;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFio() {
		return fio;
	}

	public void setFio(String fio) {
		this.fio = fio;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@ManyToOne
	@JoinColumn(name = "id_unit")
	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	/**
	 * ��������� ���������, �� ������� ��������� �������� �������������.
	 * 
	 * @return
	 */
	@OneToMany(mappedBy = "responsible", cascade = CascadeType.ALL)
	public Set<Conference> getConferenceResponce() {
		return conferenceResponce;
	}

	public void setConferenceResponce(Set<Conference> conferenceResponce) {
		this.conferenceResponce = conferenceResponce;
	}

	/**
	 * ��������� ���������, � ������� ��������� ��������� �������.
	 * 
	 * @return
	 */
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JoinTable(name = "employee_conference", 
	joinColumns = { @JoinColumn(name = "id_employee", nullable = false, updatable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "id_conference", nullable = false, updatable = false) })
	public Set<Conference> getConferenceParticipare() {
		return conferenceParticipare;
	}

	public void setConferenceParticipare(Set<Conference> conferenceParticipare) {
		this.conferenceParticipare = conferenceParticipare;
	}
	
	@Transient
	public int getAge() {
	    Calendar dob = Calendar.getInstance();
	    Calendar today = Calendar.getInstance();
	 
	    dob.setTime(birthday);
	    dob.add(Calendar.DAY_OF_MONTH, -1);
	 
	    int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
	    if (today.get(Calendar.DAY_OF_YEAR) <= dob.get(Calendar.DAY_OF_YEAR)) {
	        age--;
	    }
	    return age;
	}
	
}
