package ru.turubarov.conference.bean;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * ����� �������������.
 * @author ���������
 *
 */

@Entity
@Table(name = "unit")
public class Unit {
	private long id;
	private String name;
	private Set<Conference> conferences;
	private Set<Employee> employees;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * �������� �������������.
	 * @return
	 */
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * ��������� ���������, �������������� ��������������.
	 * @return
	 */
	@OneToMany(mappedBy = "organizer", cascade = CascadeType.ALL)
	public Set<Conference> getConferences() {
		return conferences;
	}
	public void setConferences(Set<Conference> conferences) {
		this.conferences = conferences;
	}
	
	/**
	 * ���������� �������������.
	 * @return
	 */
	@OneToMany(mappedBy = "unit", cascade = CascadeType.ALL)
	public Set<Employee> getEmployees() {
		return employees;
	}
	public void setEmployees(Set<Employee> employees) {
		this.employees = employees;
	}
}
