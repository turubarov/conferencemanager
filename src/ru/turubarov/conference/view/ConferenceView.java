package ru.turubarov.conference.view;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ����� ��� ���������� �������� ������ �� �������� ������ ���������. 
 * @author ���������
 *
 */
public class ConferenceView {
	private String subject;
	private int idUnit;
	private int idEmployee;
	private Date startOfRange;
	private Date endOfRange;
	
	/**
	 * ��������� ��� ������ ��������� �� ����.
	 * @return
	 */
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * ������������� �������������-������������ ���������.
	 * @return
	 */
	public int getIdUnit() {
		return idUnit;
	}
	public void setIdUnit(int idUnit) {
		this.idUnit = idUnit;
	}
	
	/**
	 * ������������� ����������, ������������ � ���������.
	 * @return
	 */
	public int getIdEmployee() {
		return idEmployee;
	}
	public void setIdEmployee(int idEmployee) {
		this.idEmployee = idEmployee;
	}
	
	/**
	 * ������ ��������� ���, �� �������� ����������� ������ ���������.
	 * @return
	 */
	public Date getStartOfRange() {
		return startOfRange;
	}
	public void setStartOfRange(Date startOfRange) {
		this.startOfRange = startOfRange;
	}
	
	/**
	 * ��������� ��������� ���, �� �������� ����������� ������ ���������.
	 * @return
	 */
	public Date getEndOfRange() {
		return endOfRange;
	}
	public void setEndOfRange(Date endOfRange) {
		this.endOfRange = endOfRange;
	}
	
	public String getStringStartOfRange() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(startOfRange);
	}
	
	public String getStringEndOfRange () {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(endOfRange);
	}
}
