package ru.turubarov.conference.services;

import java.util.List;

import ru.turubarov.conference.bean.Unit;
import ru.turubarov.conference.dao.UnitDAO;

public class UnitService {

	private static UnitDAO unitDao;

	public UnitService() {
		unitDao = new UnitDAO();
	}

	public List<Unit> findAll() {
		unitDao.openCurrentSession();
		List<Unit> books = unitDao.findAll();
		unitDao.closeCurrentSession();
		return books;
	}
}
