package ru.turubarov.conference.services;

import java.util.Date;
import java.util.List;

import ru.turubarov.conference.bean.Conference;
import ru.turubarov.conference.dao.ConferenceDAO;
import ru.turubarov.conference.view.ConferenceView;

public class ConferenceService {
	private static ConferenceDAO conferenceDao;

	public ConferenceService() {
		conferenceDao = new ConferenceDAO();
	}

	public List<Conference> findAll() {
		conferenceDao.openCurrentSession();
		List<Conference> conferences = conferenceDao.findAll();
		conferenceDao.closeCurrentSession();
		return conferences;
	}
	
	public Conference findById(long id) {
		conferenceDao.openCurrentSession();
		Conference result = conferenceDao.findById(id);
		conferenceDao.closeCurrentSession();
		return result;
	}
	
	public List<Conference> findByParameters(ConferenceView cView) {
		conferenceDao.openCurrentSession();
		List<Conference> conferences = conferenceDao.findByParameters(cView);
		conferenceDao.closeCurrentSession();
		return conferences;
	}
	
	public void addEmployeeToConference(long confererenceId, long employeeId) {
		conferenceDao.openCurrentSessionwithTransaction();
		conferenceDao.addEmployeeToConference(confererenceId, employeeId);
		conferenceDao.closeCurrentSessionwithTransaction();
	}
	
	public void removeEmployeeToConference(long confererenceId, long employeeId) {
		conferenceDao.openCurrentSessionwithTransaction();
		conferenceDao.removeEmployeeToConference(confererenceId, employeeId);
		conferenceDao.closeCurrentSessionwithTransaction();		
	}
	
	public void updateConference(long id, long unitId, long employeeId, 
			String subject, Date dateOfConference) {
		conferenceDao.openCurrentSessionwithTransaction();
		conferenceDao.updateConference(id, unitId, employeeId, subject, dateOfConference);
		conferenceDao.closeCurrentSessionwithTransaction();		
	}
	
}
