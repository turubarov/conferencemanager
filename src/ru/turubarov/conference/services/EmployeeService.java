package ru.turubarov.conference.services;

import java.util.List;

import ru.turubarov.conference.bean.Employee;
import ru.turubarov.conference.dao.EmployeeDAO;

public class EmployeeService {
	private static EmployeeDAO employeeDao;

	public EmployeeService() {
		employeeDao = new EmployeeDAO();
	}

	public List<Employee> findAll() {
		employeeDao.openCurrentSession();
		List<Employee> employees = employeeDao.findAll();
		employeeDao.closeCurrentSession();
		return employees;
	}

	/**
	 * �������� ���������� � �������� ���������������.
	 * @param id
	 * @return
	 */
	public Employee findById(long id) {
		employeeDao.openCurrentSession();
		Employee result = employeeDao.findById(id);
		employeeDao.closeCurrentSession();
		return result;
	}
}
